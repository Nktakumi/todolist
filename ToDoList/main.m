//
//  main.m
//  ToDoList
//
//  Created by Thiago van Dieten  on 04-12-13.
//  Copyright (c) 2013 Thiago van Dieten . All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XYZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XYZAppDelegate class]));
    }
}

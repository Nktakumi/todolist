//
//  XYZAppDelegate.h
//  ToDoList
//
//  Created by Thiago van Dieten  on 04-12-13.
//  Copyright (c) 2013 Thiago van Dieten . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
